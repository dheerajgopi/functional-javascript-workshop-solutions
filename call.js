function duckCount() {
    var args = Array.prototype.slice.call(arguments);

    return args.filter(function (obj) {
        return Object.prototype.hasOwnProperty.call(obj, 'quack');
    }).length;
    }

module.exports = duckCount;
