function getDependencies(tree, dependencyList) {
    dependencyList = dependencyList || [];
    var deps = tree.dependencies || [];

    Object.keys(deps).forEach(function(depName) {
        var depVersion = depName + '@' + tree.dependencies[depName].version;
        if (dependencyList.indexOf(depVersion) === -1) {
            dependencyList.push(depVersion);
        }
        getDependencies(tree.dependencies[depName], dependencyList);
    });
    return dependencyList.sort();
}

module.exports = getDependencies;
