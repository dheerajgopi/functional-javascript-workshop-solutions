module.exports = function arrayMap(arr, fn) {
    var res = [];
    for (i = 0; i < arr.length; i++) {
        res.push(fn(arr[i]));
    }
    return res;
};
