function loadUsers(userIds, load, done) {
    var users = [];
    userIds.forEach(function (usrid, index) {
        load(usrid, function (usr) {
            users[index] = usr;
            if (index === userIds.length) {
                return done(users);
            }
        });
    });
}

module.exports = loadUsers;
