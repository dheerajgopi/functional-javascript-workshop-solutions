function curryN(fn, n) {
    if (typeof n != 'number') {
        n = fn.length;
    }

    function curriedfn(prevArgs) {
        return function (arg) {
            var args = prevArgs.concat(arg);
            if (args.length < n) {
                return curriedfn(args);
            }
            else {
                return fn.apply(this, args);
            }
        };
    }

    return curriedfn([]);
}

module.exports = curryN;
