 function Spy(target, method) {
    var defaultFunction = target[method];
    spy = {count: 0};

    target[method] = function () {
        spy.count++;
        return defaultFunction.apply(this, arguments);
    };

    return spy;
}

module.exports = Spy;
