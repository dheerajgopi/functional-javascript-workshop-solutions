function countWords(inputWords) {
    return inputWords.reduce(function (obj, val) {
        if (typeof(obj[val]) == 'undefined') {
            obj[val] = 1;
        }
        else {
            obj[val] = ++obj[val];
        }
        return obj;
    }, {});
}

module.exports = countWords;
